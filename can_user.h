
#ifndef  _CAN_USER_H_
#define  _CAN_USER_H_

#include "main.h"

// defines
#define  CAN1_TX_QUEUE_SIZE	8
#define  CAN1_RX_QUEUE_SIZE	16



// Baudrates
#if CPU_CLOCK==60000000
// VPB clock 60 MHz, 15 Tsegs, sample point 80 %
//									-- SJW --	- Tseg1 -	- Tseg2 -	- BRP -
#define		CAN_BAUD_1M			(	0 << 14 |	10 << 16 |	2 << 20 |	3)
#define		CAN_BAUD_800K		(	0 << 14 |	10 << 16 |	2 << 20 |	4)
#define		CAN_BAUD_500K		(	0 << 14 |	10 << 16 |	2 << 20 |	7)
#define		CAN_BAUD_250K		(	0 << 14 |	10 << 16 |	2 << 20 |	15)
#define		CAN_BAUD_200K		(	0 << 14 |	10 << 16 |	2 << 20 |	19)
#define		CAN_BAUD_125K		(	0 << 14 |	10 << 16 |	2 << 20 |	31)
#define		CAN_BAUD_100K		(	0 << 14 |	10 << 16 |	2 << 20 |	39)
#define		CAN_BAUD_95K2		(	0 << 14 |	10 << 16 |	2 << 20 |	41)
#define		CAN_BAUD_83K3		(	0 << 14 |	10 << 16 |	2 << 20 |	47)
#define		CAN_BAUD_50K		(	0 << 14 |	10 << 16 |	2 << 20 |	79)
#define		CAN_BAUD_47K6		(	0 << 14 |	10 << 16 |	2 << 20 |	83)
#define		CAN_BAUD_33K3		(	0 << 14 |	10 << 16 |	2 << 20 |	119)
#define		CAN_BAUD_20K		(	0 << 14 |	10 << 16 |	2 << 20 |	199)
#define		CAN_BAUD_10K		(	0 << 14 |	10 << 16 |	2 << 20 |	399)
#endif

#if CPU_CLOCK==48000000
// VPB clock 48 MHz, 15 Tsegs, sample point 87 %
//									-- SJW --	- Tseg1 -	- Tseg2 -	- BRP -
#define		CAN_BAUD_1M			(	0 << 14 |	 4 << 16 |	1 << 20 |	5)
#define		CAN_BAUD_800K		(	0 << 14 |	 6 << 16 |	1 << 20 |	5)
#define		CAN_BAUD_500K		(	0 << 14 |	12 << 16 |	1 << 20 |	5)
#define		CAN_BAUD_250K		(	0 << 14 |	12 << 16 |	1 << 20 |	11)
#define		CAN_BAUD_200K		(	0 << 14 |	14 << 16 |	3 << 20 |	11)
#define		CAN_BAUD_125K		(	0 << 14 |	12 << 16 |	1 << 20 |	23)
#define		CAN_BAUD_100K		(	0 << 14 |	15 << 16 |	2 << 20 |	23)
#define		CAN_BAUD_95K2		(	1 << 14 |	14 << 16 |	4 << 20 |	23)
#define		CAN_BAUD_83K3		(	1 << 14 |	12 << 16 |	1 << 20 |	35)
#define		CAN_BAUD_50K		(	2 << 14 |	15 << 16 |	2 << 20 |	47)
#define		CAN_BAUD_47K6		(	2 << 14 |	 9 << 16 |	2 << 20 |	71)
#define		CAN_BAUD_33K3		(	2 << 14 |	11 << 16 |	2 << 20 |	89)
#define		CAN_BAUD_20K		(	2 << 14 |	15 << 16 |	2 << 20 |	119)
#define		CAN_BAUD_10K		(	3 << 14 |	15 << 16 |	2 << 20 |	239)
#endif


// user function protos

CANStatus_t  CAN_UserWrite ( CANHandle_t  hBus, CANMsg_t  *pBuff);


u32_t  CAN_UserRead ( CANHandle_t  hBus, CANMsg_t  *pBuff);
u32_t CAN_Filter (void* Msg);


void  CAN_UserInit ( void);


#endif
