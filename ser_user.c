

#include "datatypes.h"
#include "main.h" // for "CPU_CLOCK"
#include "serial.h"
#include "ser_user.h"

// TX Fifo (soft-fifo read by TX complete interrupt)
static u8_t  TxFifo[SER_TX_FIFO_SIZE];

// RX Fifo (soft-fifo write by RX complete interrupt)
static u8_t  RxFifo[SER_RX_FIFO_SIZE];


// Init the serial interface
void  SER_UserInit (u32_t SerSpeed)
{

	SERInit_t  setup;
	
 	setup.prescaler = ( CPU_CLOCK + 8 * SerSpeed) / ( 16 * SerSpeed);	
//	setup.baudrate = (SerSpeed*30)/24; // (* 60 MHz / 48 MHz), since Clock was reduced.
	setup.databits = 8;
	setup.stopbits = 1;
	setup.parity = SER_PARITY_NONE;
	
	setup.pTxFifo = &TxFifo;
	setup.pRxFifo = &RxFifo;
	
	setup.TxFifoSize = SER_TX_FIFO_SIZE;
	setup.RxFifoSize = SER_RX_FIFO_SIZE;
	
	setup.ISRnum = 5;		// VIC channels 0 to 4 used for CAN
	
	SER_Initialize ( SER_PORT1, &setup);
	
}

u8_t hex2ascii(u8_t chr)
{
	chr = chr & 0xF;
	if(chr > 9)
	{
		chr += 0x37;
	}
	else
	{
		chr += 0x30;
	}
	return chr;
}

u8_t ascii2hex(u8_t chr)
{
	if((chr <= 0x5A) && (chr >= 0x41)) // Großbuchstaben
	{
		chr -= 0x37;
		return chr &0xF;
	}
	if((chr <= 0x7A) && (chr >= 0x61)) // Kleinbuchstaben
	{
		chr -=0x57;
		return chr &0xF;
	}
	
	if((chr <= 0x39) && (chr >= 0x30)) // Zahlen
	{
		chr -= 0x30;
	}
	return chr & 0xF;
}

