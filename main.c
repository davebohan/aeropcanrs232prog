/**
 * @file 	Main.c
 * @brief 	Contains initialization of the hardware and the main scheduler.
 * @author 	StS
 * @date 	16 Jan. 2012
 * @version	0.0.1
 */

//-------------------------------------------------
//! For this project, 
//! uC clock was reduced from 5* 12MHz to 4* 12MHz
//! for being compatible to a 16MHz clocked SJA-1000.
//-------------------------------------------------

#include "lpc21xx.h"
#include "datatypes.h"
#include "systime.h"
#include "can.h"
#include "can_user.h"
#include "hardware.h"
#include "crc_data.h"
#include "PCAN-RS-232.h"
#include "help.h"

// Global variables
u16_t Millitimer = 0;
u32_t TimeDiff1000Hz = 0;
u32_t LedTimeDiff = 0;


// identifier is needed by PCANFlash.exe -> do not change or delete
const b8_t Ident[] __attribute__ ((used)) = { "PCAN-RS-232"};


// info data for PCANFlash.exe
const crc_array_t  C2F_Array __attribute__((section(".C2F_Info"), used)) = {

	.Str = CRC_IDENT_STRING,
	.Version = 0x21,
	.Day = 5,
	.Month = 5,
	.Year = 6,
	.Mode = 1,

	// crc infos are patched during link time by file "flash.ld"
	// crc value is patched by Tool "PCANFlash.exe"
};

//-------------------------------------------------

// main()
// entry point from crt0.S
int main (void)
{
	static u8_t BlinkState = 0; // local variable
	
	HW_Init();
	SYSTIME_Init();
	PCANRS232_Init(); // Read operation parameters from EEPROM and setup the device accordingly
	
	// main loop
	while (1) 
	{
		//-------------------------------------------------
		// millisec timer for CAN receive timestamp
		if (SYSTIME_DIFF (TimeDiff1000Hz, SYSTIME_NOW) > 1000){
//			#if testtimer
//			if(SYSTIME_DIFF (TimeDiff1000Hz, SYSTIME_NOW) > 2000)
//			{
//				system_too_slow++;
//			}
//			#endif
			Millitimer++;
//			Millitimer+=(SYSTIME_DIFF (TimeDiff1000Hz, SYSTIME_NOW)/1000);
			if(Millitimer>60000) Millitimer=0;
			//TimeDiff1000Hz+=1000;
			TimeDiff1000Hz=SYSTIME_NOW;
		}
		//-------------------------------------------------

		// 1Hz LED Blinker
		if (SYSTIME_DIFF (LedTimeDiff, SYSTIME_NOW) > 500000){
			if (EepromData.CanChnOpen == 1) {
				// toggle LED
				if (BlinkState != HW_LED_GREEN) {
					BlinkState = HW_LED_GREEN;
				}
				else {						
					BlinkState = HW_LED_OFF;
				}
				HW_SetLED (HW_LED_CAN1, BlinkState);
			}//IF: CAN chan. open
			else { // CAN chn. closed, set green static
				HW_SetLED (HW_LED_CAN1, HW_LED_GREEN);
			}
			LedTimeDiff = SYSTIME_NOW;
		}//IF: SYSTIME_DIFF
		//-------------------------------------------------

		ProcessMsgFromCan(); // those who passed the acceptance filter
		//ProcessMsgFromSerial(); // commands from RS232 UART (Host-PC)
	}
}
