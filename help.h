// for doxygen documentation generator only !!

/**

@mainpage
<img src="PCAN-RS-232.jpg">


<h2>Introduction</h2>

This document describes use and interna of the PCAN-RS-232 shipping firmware.
Project files and source code for building this (along with necessary tools) is included on CD.
<br><br>

<h3>PCAN-RS-232: Scope of delivery</h3>
- PCAN-RS-232 hardware
- hardware manual
- WinARM GCC compiler package
- programming examples with complete source code and documentation
- Flash programming software
<br><br>

<h3>Other useful equipment (optional)</h3>
- A PCAN PC interface for uploading your own firmware to the PCAN-RS-232
- CAN cable and termination
- CAN hacker (shareware)
<br><br>

<h3>Background and innovations</h3>
The underlying programming library <b>"libPCAN-RS-232GNU1.0.1ys.a"</b> 
was ported from other well-known PEAK microcontroller platforms and allows in-depth access 
to the PCAN-RS-232's hardware resources like CAN, EEPROM etc.
<br><br>

<h3>Document version information</h3>
    <b>v1.0.0</b> (as of 16. Jan. 2012) by StS <br>
(c) 2012 PEAK-System Technik GmbH, Darmstadt

*/

