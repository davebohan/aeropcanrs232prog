
#include "datatypes.h"
#include "lpc21xx.h"
#include "can.h"
#include "can_user.h"
#include "hardware.h"
#include "PCAN-RS-232.h"


// Queues for CAN1
CANMsg_t  TxQueueCAN1[CAN1_TX_QUEUE_SIZE];
CANMsg_t  RxQueueCAN1[CAN1_RX_QUEUE_SIZE];

#define DUAL_FILTER 0 
#define SINGLE_FILTER 1

//*************************************************





// CAN_UserWrite()
// Send a message on CAN_BUSx
CANStatus_t  CAN_UserWrite ( CANHandle_t  hBus, CANMsg_t  *pBuff)
//-------------------------------------------------
{
	CANStatus_t  ret;
	CANMsg_t  *pMsg;
	
	
	ret = CAN_ERR_OK;
	
	pMsg = CAN_TxQueueGetNext ( hBus);

	if ( pMsg != NULL)
	{
		pMsg->Id   = pBuff->Id;
		pMsg->Len  = pBuff->Len;
		pMsg->Type = pBuff->Type;
		
		pMsg->Data32[0] = pBuff->Data32[0];
		pMsg->Data32[1] = pBuff->Data32[1];
		
		// Send Msg
		ret = CAN_TxQueueWriteNext ( hBus);
	}
	
	else
	{
		// Tx Queue FULL
		ret = CAN_ERR_FAIL;
	}
	
	return ret;
}
//*************************************************





// CAN_UserRead()
// read message from CAN_BUSx
u32_t  CAN_UserRead ( CANHandle_t  hBus, CANMsg_t  *pBuff)
//-------------------------------------------------
{
	u32_t  ret;
	CANMsg_t  *pMsg;
	
	
	ret = 0;
	
	pMsg = CAN_RxQueueGetNext ( hBus);

	if ( pMsg != NULL)
	{
		pBuff->Id   = pMsg->Id;
		pBuff->Len  = pMsg->Len;
		pBuff->Type = pMsg->Type;
		
		pBuff->Data32[0] = pMsg->Data32[0];
		pBuff->Data32[1] = pMsg->Data32[1];
		
		CAN_RxQueueReadNext ( hBus);
		ret = 1;
	}
	
	return ret;
}
//*************************************************





// CAN_UserInit()
// initialize CAN1
void  CAN_UserInit ( void)
//-------------------------------------------------
{
	// init CAN1

	CAN_ReferenceTxQueue ( CAN_BUS1, &TxQueueCAN1[0], CAN1_TX_QUEUE_SIZE);				// Reference above Arrays as Queues
	CAN_ReferenceRxQueue ( CAN_BUS1, &RxQueueCAN1[0], CAN1_RX_QUEUE_SIZE);

	CAN_SetTimestampHandler ( CAN_BUS1, NULL);

	VICVectAddr1 = (u32_t) CAN_GetIsrVector ( CAN1_TX_INTSOURCE);
	VICVectAddr3 = (u32_t) CAN_GetIsrVector ( CAN1_RX_INTSOURCE);

	VICVectCntl1 = 1 << 5 | CAN1_TX_INTSOURCE;											// Setup VIC
	VICVectCntl3 = 1 << 5 | CAN1_RX_INTSOURCE;

	VICIntEnable = 1 << CAN1_TX_INTSOURCE | 1 << CAN1_RX_INTSOURCE;

	CAN_SetErrorLimit ( CAN_BUS1, STD_TX_ERRORLIMIT);

	CAN_SetTxErrorCallback ( CAN_BUS1, NULL);											// Set ErrorLimit & Callbacks

	// Decide by Acceptance filter, whether to keep or to discard the message
	CAN_SetRxCallback (CAN_BUS1, &CAN_Filter);

	CAN_SetChannelInfo ( CAN_BUS1, NULL);													// Textinfo is NULL


	// Set Error Handler

	VICVectAddr0 = (u32_t) CAN_GetIsrVector ( GLOBAL_CAN_INTSOURCE);
	VICVectCntl0 = 1 << 5 | GLOBAL_CAN_INTSOURCE;
	VICIntEnable = 1 << GLOBAL_CAN_INTSOURCE;


	// Setup Filters

	CAN_InitFilters();										// Clear Filter LUT
	CAN_SetFilterMode ( AF_ON_BYPASS_ON);				// No Filters ( Bypassed)

	//
	CAN_SetTransceiverMode (CAN_BUS1, CAN_TRANSCEIVER_MODE_NORMAL);
}
//*************************************************





u32_t CAN_Filter (void* RxMsg)
//-------------------------------------------------
// result LEAVE_MESSAGE: message will pass filter and is kept in Rx queue 
// result  SKIP_MESSAGE: message won't pass filter and is removed from Rx queue
 {
	CANMsg_t* Msg;
	u32_t message_bits;
	u32_t acc_code_bits;
	u32_t acc_mask_bits;
	u32_t data_mask=0;

//	u32_t message_bits_df;
	u32_t acc_code_bits1;
	u32_t acc_code_bits2;
	u32_t acc_mask_bits1;
	u32_t acc_mask_bits2;
	u32_t data_mask1=0;
	u32_t data_mask2=0;
	
	Msg = RxMsg;

	if (EepromData.FilterMode == DUAL_FILTER) {
		if (Msg->Type & CAN_MSG_EXTENDED) {	// 29bit dual filter
			// work with lower 16 bit of a 32bit variable
			message_bits = Msg->Id >>13;  // shift id bits 28-13 to bitpos 15-0 (16 bit result)
			
			// Filter 1:
			acc_code_bits1 = ((u32_t)EepromData.AcceptanceCode[0] <<8) + EepromData.AcceptanceCode[1];
			acc_mask_bits1 = ((u32_t)EepromData.AcceptanceMask[0] <<8) + EepromData.AcceptanceMask[1];
			data_mask1 = 0xffff0000; // upper 16 bits not used, so fixed to don't care

			if (((~message_bits^acc_code_bits1) | acc_mask_bits1 | data_mask1) == 0xffffffff) {
				return LEAVE_MESSAGE; // Message will pass filter 1 
			}
			// Filter 2:
			acc_code_bits2 = ((u32_t)EepromData.AcceptanceCode[2] <<8) + EepromData.AcceptanceCode[3];
			acc_mask_bits2 = ((u32_t)EepromData.AcceptanceMask[2] <<8) + EepromData.AcceptanceMask[3];
			data_mask2 = 0xffff0000; // upper 16 bits not used, so fixed to don't care
			
			if (((~message_bits^acc_code_bits2) | acc_mask_bits2 | data_mask2) == 0xffffffff) {
				return LEAVE_MESSAGE; // Message will pass filter 2 
			}
			return SKIP_MESSAGE; // Message discarded
		}
//-------------------------------------------------
		else {	// 11 bit dual filter
			// work with lower 20 bit of a 32bit variable
			message_bits = Msg->Id <<9;
			// set RTR-Bit if necessary
			if (Msg->Type & CAN_MSG_RTR) {
				message_bits |= 0x100;
			}
			message_bits |= Msg->Data8[0]; // compare also the first data byte of the message

			// Filter 1:
			acc_code_bits1 = ((u32_t)EepromData.AcceptanceCode[0] <<12) + ((u32_t)EepromData.AcceptanceCode[1] <<4) + (EepromData.AcceptanceCode[3] & 0x0f);
			acc_mask_bits1 = ((u32_t)EepromData.AcceptanceMask[0] <<12) + ((u32_t)EepromData.AcceptanceMask[1] <<4) + (EepromData.AcceptanceMask[3] & 0x0f);
			data_mask1 = 0xfff00000; // unused bits of 32bit-value set to 1

			if ((Msg->Len == 0) || (Msg->Type & CAN_MSG_RTR)) {
				data_mask1 |= 0xff;	// more bits don't care depending on len and RTR
			}
		
			if (((~message_bits^acc_code_bits1) | acc_mask_bits1 | data_mask1) == 0xffffffff) {
				return LEAVE_MESSAGE; // Message will pass filter
			}
	
			// Filter 2:
			acc_code_bits2 = ((u32_t)EepromData.AcceptanceCode[2] <<12) + ((EepromData.AcceptanceCode[3] & 0xf0) <<4);
			acc_mask_bits2 = ((u32_t)EepromData.AcceptanceMask[2] <<12) + ((EepromData.AcceptanceMask[3] & 0xf0) <<4);
			data_mask2 = 0xfff000ff; // unused bits of 32bit-value set to 1

			if (((~message_bits^acc_code_bits2) | acc_mask_bits2 | data_mask2) == 0xffffffff) {
				return LEAVE_MESSAGE; // Message will pass filter
			}
			return SKIP_MESSAGE; // Message discarded
		}
	}
	//-------------------------------------------------
	else {	// Single Filter
		acc_code_bits = ((u32_t)EepromData.AcceptanceCode[0] <<24) + ((u32_t)EepromData.AcceptanceCode[1] <<16) + ((u32_t)EepromData.AcceptanceCode[2] <<8) + (u32_t)EepromData.AcceptanceCode[3];
		acc_mask_bits = ((u32_t)EepromData.AcceptanceMask[0] <<24) + ((u32_t)EepromData.AcceptanceMask[1] <<16) + ((u32_t)EepromData.AcceptanceMask[2] <<8) + (u32_t)EepromData.AcceptanceMask[3];
		// 29 bit ID
		if(Msg->Type & CAN_MSG_EXTENDED) {	// = 29 bit ID
			data_mask    |= 0x03; 			// lower 2 bits not used
			message_bits  = Msg->Id <<3;	// left-adjust bits
			message_bits &= 0xfffffff8; 	// clear lower 3 bits
			// set RTR-Bit if necessary
			if (Msg->Type & CAN_MSG_RTR) {
				message_bits |= 4; 
			}
		}
	//-------------------------------------------------
		else { 								// = 11 bit ID
			data_mask    |= 0x000f0000;		// unused bits
			message_bits  = Msg->Id<<21;	// left-adjust bits
			message_bits &= 0xffe00000; 	// clear lower 21 bits

			 // compare also the first and the second data byte of the message
			message_bits |=(Msg->Data8[0]<<8); 
			message_bits |=(Msg->Data8[1]);

			// set RTR-Bit if necessary
			if(Msg->Type & CAN_MSG_RTR) {
				message_bits |= 0x100000; 
			}
			if ((Msg->Len == 0) || (Msg->Type & CAN_MSG_RTR)) {
				data_mask |= 0x0000ffff;	// mask unused databytes 1 + 2 (when len=0 or when RTR frame)
			}
			else if (Msg->Len == 1) {
				data_mask |= 0x000000ff; 	// mask unused databyte 2 (only databyte 1 to be compared)
			}
			else {
				data_mask |= 0;				// do not mask, since both databytes must be compared
			}
		}
		//if (((message_bits^acc_code_bits) | acc_mask_bits | data_mask) == 0xffffffff) {
		if (((~message_bits^acc_code_bits) | acc_mask_bits | data_mask) == 0xffffffff) {
			return LEAVE_MESSAGE; // Message will pass filter
		}
	}
	return SKIP_MESSAGE; // Message discarded
}
//*************************************************
