#ifndef _MAIN_H_
#define _MAIN_H_

/**
 * @file 	Main.h
 * @brief 	Contains global definitions published from the MAIN module.
 * @author 	S. Schott
 * @date 	16 Jan. 2012
 * @version	0.0.1
 */

//
//	main.h
//

#define CPU_CLOCK 48000000 //!< This example sets hardware running with 48MHz

extern u32_t LedTimeDiff;
extern u16_t Millitimer;

#endif
