#define __PCANRS232_C__

/**
 * @file 	PCAN-RS-232.c
 * @brief 	This example code implements the CAN-to-serial-by-command application.
 * PCAN-RS-232 is shipped with this firmware in flash memory.
 * 
 * First time use:
 * When powering up the PCAN-RS-232 will blink 3 times red, meaning the serial interface is set to 57600 bit/s. 
 * CAN is configured to 125 kbit/s, all filters are inactive.
 * When e.g. starting the popular CAN hacker shareware, one might immediately send and receive CAN messages. 
 * The PCAN-RS-232 device behaves widely as similar adapters from other (famous) brands...
 *
 * Modified from the original example/6_CAN_TO_SER_BY_COMMAND.
 * Purpose: Communicate between the CR1081 and the SST_LV.
 * 
 *
 * @author 	D Bohan
 * @date 	06 Dec. 2017
 * @version	1.1.1
 */

//*********************************************************************
//  Title   : PCAN-RS-232.c
//
//	Autor   : StS
//	 
//
//  Project : PCAN-RS-232
//
//  Function:
//
//*********************************************************************


// Note: 
// All commands to the PCAN-RS-232 must end with [CR] (Ascii=13) 
// All commands are CASE sensitive.
//
// Examples:
// ----------
// [CR]
// [CR]
// [CR]
// V[CR] (should reply HW+SW version, e.g. V1324[CR])
// S4[CR] (set CAN speed to 125Kbit)
// Z1[CR] Enable timestamp feature
// Q1[CR] Turn ON the Auto Startup feature in normal mode
// O[CR] (open the CAN channel) -> the LED should be green, blinking
// t1001AA[CR] (sends ID=0x100 hex with DLC=1 and data 0xAA, one byte)
// C[CR] (close the CAN channel) -> the LED should be green, static)
// ----------

#include "lpc21xx.h"
#include "datatypes.h"
#include "hardware.h"
#include "systime.h"
#include "eeprom.h"
#include "can.h"
#include "serial.h"
#include "can_user.h"
#include "ser_user.h"
#include "pcan-rs-232.h"
#include <string.h> // for memcpy in GetSerialNumber


//*************************************************
// global data
//*************************************************

u8_t REVISION = 1;

#define ret_OK    0xD // [CR]   "\r"
#define ret_ERROR 0x7 // [Bell] "\a"

// max. serial buffer length: 29bit-frame plus timestamp plus \r plus trailing zero
#define buflen (sizeof("T1FFFFFFF81122334455667788EA5F\r")+1)
u8_t	SerXmtBuf[buflen]; // will assemble and send data to the Host-PC / RS232
u8_t	SerXmtBufPtr = 0;
u8_t	SerRcvBuf[buflen]; // holds commands from the Host-PC / RS232
u8_t	SerRcvBufPtr = 0;

typedef struct _sja1000bits {
u8_t bit0:1; // CAN receive FIFO queue full
u8_t bit1:1; // CAN transmit FIFO queue full
u8_t bit2:1; // Error warning (EI), see SJA1000 datasheet
u8_t bit3:1; // Data Overrun (DOI), see SJA1000 datasheet
u8_t bit4:1; // Not used.
u8_t bit5:1; // Error Passive (EPI), see SJA1000 datasheet
u8_t bit6:1; // Arbitration Lost (ALI), see SJA1000 datasheet
u8_t bit7:1; // Bus Error (BEI), see SJA1000 datasheet
} sja1000bits;

typedef /*struct*/ union {
sja1000bits statusbits;
u8_t 		statusbyte;
} reg;

EepromDataStruct EepromData;

CANMsg_t  CanRxMsg, CanTxMsg;

static u32_t BlinkTimeDiff = 0;
// Write to UART was either ok or TX queue was full (repeat in latter case)
SERStatus_t UARTWriteResult;

// Returned to user when requesting Statusflags (command F) 
u8_t SerRxOverrunOccurred;
u8_t SerTxCongestionOccurred;

u8_t RelayState;

u32_t Bitrates[]=
{
	CAN_BAUD_10K,
	CAN_BAUD_20K,
	CAN_BAUD_50K,
	CAN_BAUD_100K,
	CAN_BAUD_125K,
	CAN_BAUD_250K,
	CAN_BAUD_500K,
	CAN_BAUD_800K,
	CAN_BAUD_1M
};

//*************************************************

// Local Function prototypes
static void SetCanSpeed(void);
static void OpenCanChannelNormal(void);
static void WriteToEeprom(u8_t ParamSetNo);
static void CheckEepromSanity(void);
static void ReadMsgFromSerial(void);

//*************************************************





//*************************************************
//! @brief	
//! Initializes software functions.
//! To be called from MAIN() once at module startup.
//!
//! @param	void
//!
//! @return	void
//!
void PCANRS232_Init(void)
//-------------------------------------------------
{
	static EEPROMStatus_t Eeprom_Ret;
	static u8_t i = 0; // all-purpose loop counter
	static u8_t LedState = 0;
	u8_t dummy;
	// set led = uninitialized
	HW_SetLED (HW_LED_CAN1, HW_LED_OFF); // OFF, GREEN, RED, ORANGE

	// read operating params from eeprom
	Eeprom_Ret = EEPROM_Read (0x100, &EepromData, EepromDataLength); 

        // Hardcode the CAN speed!!
        EepromData.CanSpeed = 4; //  CAN_BAUD_125K

	// EEPROM Read successful
	if (Eeprom_Ret == EEPROM_ERR_OK) {

		// Check EEPROM for implausible values
		CheckEepromSanity();
	
		if (EepromData.AutostartMode == 0) {
			EepromData.CanBusMode = BUS_OFF;  // OFF
			EepromData.CanChnOpen = 0;
		}
		else if ((EepromData.AutostartMode == 1)  ||(EepromData.AutostartMode == 2)) { // CAN normal/LOM
			CAN_UserInit();
			//EepromData.CanBusMode = BUS_ON;

			if(EepromData.CanSpeed<=8)  // set one of the predefined bitrates
			{
				CAN_InitChannel(CAN_BUS1, Bitrates[EepromData.CanSpeed]);
			}
			else if (EepromData.CanSpeed==0xff)	// set user bitrate
			{
				u32_t LpcBtr;
				LpcBtr  = ((((u32_t)EepromData.Btr[0] &0x3F) +1) *6) -1; 	// BRP, bit 0..9
				LpcBtr |=   ((u32_t)EepromData.Btr[0] &0xC0) << 8;			// SJW, bit 14..15
				LpcBtr |=   ((u32_t)EepromData.Btr[1]       << 16);			// TSEG1, TSEG2, SAM
				// Write values to CAN controller
				CAN_InitChannel (CAN_BUS1, LpcBtr);

			}
			//CAN_SetBusMode (CAN_BUS1, EepromData.CanBusMode); // CAN Bus On or LOM
			//EepromData.CanChnOpen = 1;
		}
	}
	else // EEPROM Read Error --> set defaults
	{
		WriteToEeprom(1); // 1 = write factory defaults to EEPROM
	}
	EepromData.CanInitialized = 1;
//-------------------------------------------------

	// init serial
	dummy=EepromData.CanChnOpen ;
	EepromData.CanChnOpen =0;
        SER_UserInit (SER_BAUD_9K6);
	//SetUartSpeed(SER_BAUD_9K6);
	//SetUartSpeed(EepromData.SerialSpeed); 	// set real speed from eeprom
	EepromData.CanChnOpen =dummy;
	SerRxOverrunOccurred = 0;				// reset error flags UART Rx
	SerTxCongestionOccurred = 0;			// reset error flags UART Tx


	// 1*  --> adapter is currently set to 230400 baud
	// 2*  --> adapter is currently set to 115200 baud
	// 3*  --> adapter is currently set to  57600 baud
	// 4*  --> adapter is currently set to  38400 baud
	// 5*  --> adapter is currently set to  19200 baud
	// 6*  --> adapter is currently set to   9600 baud
	// 7*  --> adapter is currently set to   2400 baud
	
	i=0;
	do { // indicate program revision to the user by blinking n times
		if (SYSTIME_DIFF (BlinkTimeDiff, SYSTIME_NOW) > 250000){
			// toggle LED
			if (LedState == HW_LED_OFF) {
				LedState = HW_LED_ORANGE;
			}
			else {						
				LedState = HW_LED_OFF;
			i++;
			}
			HW_SetLED (HW_LED_CAN1, LedState);
			BlinkTimeDiff = SYSTIME_NOW;
		}//IF: SYSTIME_DIFF
	} //DO
	while (i < REVISION);
        SYSTIME_WaitMicros(1000000);  // wait 1 sec
//-------------------------------------------------

	// init CAN , only if not already initialized due to Autostart mode
	if(EepromData.AutostartMode == 0)
	{
		CAN_UserInit();
		// set can bitrate from EepromData.CanSpeed
		SetCanSpeed(); 
	}

	// set bus mode
	CAN_SetBusMode (CAN_BUS1, EepromData.CanBusMode); // CAN Bus On
	// at normal startup (-not- autostart) open can filter always (regardless of eeprom settings)
	if (EepromData.AutostartMode == 0) {
		EepromData.AcceptanceCode[0]= 0x00;
		EepromData.AcceptanceCode[1]= 0x00;
		EepromData.AcceptanceCode[2]= 0x00;
		EepromData.AcceptanceCode[3]= 0x00;
		EepromData.AcceptanceMask[0]= 0xFF;
		EepromData.AcceptanceMask[1]= 0xFF;
		EepromData.AcceptanceMask[2]= 0xFF;
		EepromData.AcceptanceMask[3]= 0xFF;
	}
	
	EepromData.CanInitialized = 1;
	// set led = initialized but not connected
	HW_SetLED ( HW_LED_CAN1, HW_LED_GREEN); // OFF GREEN RED ORANGE

        // Initialize PCAN-RS_232
        // Set alarm setpoints 1 and 2 to values that will never be reached
        //   to gaurantee that the alarm will never activate.

        // Setpoint 1
        SerXmtBuf[0] = '*';  // All commands begin with '*'
        SerXmtBuf[1] = '1';  // device address
        SerXmtBuf[2] = 'F';  // Write Lower RAM
        SerXmtBuf[3] = '3';  // Length: 3 bytes
        SerXmtBuf[4] = '8';
        SerXmtBuf[5] = '6';  // Address 0x86
        SerXmtBuf[6] = '0';
        SerXmtBuf[7] = 'F';  // Address 0x86 = 0xF0
        SerXmtBuf[8] = '0';
        SerXmtBuf[9] = '0';  // Address 0x85 = 0x00
        SerXmtBuf[10]= '0';
        SerXmtBuf[11]= '0';  // Address 0x84 = 0x00
        SerXmtBuf[12]= '\r';
        SerXmtBufPtr = 13;

        // Send ASCII string to RS232 UART (Host-PC)
        UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
        SYSTIME_WaitMicros(10000);  // wait 10 ms (May not be necessary

        // Setpoint 2
        SerXmtBuf[0] = '*';  // All commands begin with '*'
        SerXmtBuf[1] = '1';  // device address
        SerXmtBuf[2] = 'F';  // Write Lower RAM
        SerXmtBuf[3] = '3';  // Length: 3 bytes
        SerXmtBuf[4] = '8';
        SerXmtBuf[5] = '9';  // Address 0x89
        SerXmtBuf[6] = '0';
        SerXmtBuf[7] = 'F';  // Address 0x89 = 0xF0
        SerXmtBuf[8] = '0';
        SerXmtBuf[9] = '0';  // Address 0x88 = 0x00
        SerXmtBuf[10]= '0';
        SerXmtBuf[11]= '0';  // Address 0x87 = 0x00
        SerXmtBuf[12]= '\r';
        SerXmtBufPtr = 13;

        // Send ASCII string to RS232 UART (Host-PC)
        UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
        SYSTIME_WaitMicros(10000);  // wait 10 ms (May not be necessary

        // Alarm 1 config
        // Relays off
        SerXmtBuf[0] = '*';  // All commands begin with '*'
        SerXmtBuf[1] = '1';  // device address
        SerXmtBuf[2] = 'Q';  // Write Upper RAM
        SerXmtBuf[3] = '1';  // Length: 1 bytes
        SerXmtBuf[4] = '0';
        SerXmtBuf[5] = 'A';  // Address 0x0A
        SerXmtBuf[6] = '0';
        SerXmtBuf[7] = '0';  // Address 0x0A = 0x00
        SerXmtBuf[8] = '\r';
        SerXmtBufPtr = 9;

        // Send ASCII string to RS232 UART (Host-PC)
        UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);

        /*
        SerXmtBuf[0] = 'C';
        SerXmtBuf[1] = 'A';
        SerXmtBuf[2] = 'N';
        SerXmtBuf[3] = ' ';
        SerXmtBuf[4] = 'B';
        SerXmtBuf[5] = 'a';
        SerXmtBuf[6] = 'u';
        SerXmtBuf[7] = 'd';
        SerXmtBuf[8] = ' ';
        SerXmtBuf[9] = '0';
        SerXmtBuf[10] = 'x';
        SerXmtBuf[11] = hex2ascii((EepromData.CanSpeed & 0xF0) >> 4);  // address upper nibble
        SerXmtBuf[12] = hex2ascii((EepromData.CanSpeed & 0x0F));       // address lower nibble
        SerXmtBuf[13] = '\r';
        SerXmtBufPtr = 14;
        
        // Send ASCII string to RS232 UART (Host-PC)
        UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
        */
        
        RelayState = 0x00;
}
//-------------------------------------------------



void ReadMsgFromSerial()
{
    SERStatus_t ReadResult;
    u8_t CurRcvChar; 	// currently received character from UART
    u8_t BytesRead;		// dummy parameter, not used here
    //u8_t CmdLength;		// length of currently received Command
    //u8_t RetValue;		// 
    u32_t counter;

    // Clear the receive buffer
    for (counter = 0; counter < buflen - 1; counter++)
        SerRcvBuf[counter] = 0;
    
    SerRcvBufPtr = 0;
    counter = 0;
    CurRcvChar = 0;
    do {
        // Read one byte from serial
        ReadResult = SER_Read (SER_PORT1, &CurRcvChar, 1, &BytesRead);
	
        // No bytes received,
        if (ReadResult == SER_ERR_RX_EMPTY){
            if (counter > 300)  // Waits up to 30 ms
                return;	 // ... quit, don't waste time
            else
            {
                SYSTIME_WaitMicros(100);
                counter++;
            }
        }	

        //-------------------------------------------------
        else if (ReadResult == SER_ERR_OK){
            counter = 0;
            SerRcvBuf[SerRcvBufPtr] = CurRcvChar;
		
            // increase pointer up to max.
            if (SerRcvBufPtr <= buflen){
                SerRcvBufPtr++;
            }
        } // else if
        //-------------------------------------------------
        else if (ReadResult == SER_ERR_RX_OVERRUN){
            SerRxOverrunOccurred = 1; // to be inserted in status register (command F)
        } // else if
        //-------------------------------------------------
    } // do
    while (CurRcvChar != '\r');
    
    //CmdLength = SerRcvBufPtr - 1; // without carriage return
}


//*************************************************
//! @brief	
//! Reads one MSG from CAN and processes it.
//! Returns a CAN message if appropriate.
//!
//!
//! @param	void
//!
//! @return	void
//!
void ProcessMsgFromCan(void) 
//-------------------------------------------------
{
    static u8_t i; //all purpose loop counter
    static u32_t inbuf;
    static u32_t outbuf;
    static u8_t opened = 0;

    if (opened == 0)
    {
        opened = 1;
        OpenCanChannelNormal();
    }
        
    if (CAN_UserRead (CAN_BUS1, &CanRxMsg) != 0) {

        switch (CanRxMsg.Id)
        {
        case 0x400:  // NVM read
            // CanRxMsg.Len ignored

            SerXmtBuf[0] = '*';  // All commands begin with '*'
            SerXmtBuf[1] = '1';  // device address
            SerXmtBuf[2] = 'X';  // NVM read
            SerXmtBuf[3] = '4';  // Length: 4 words (8 bytes)
            SerXmtBuf[4] = hex2ascii((CanRxMsg.Data8[0] & 0xF0) >> 4);  // address upper nibble
            SerXmtBuf[5] = hex2ascii((CanRxMsg.Data8[0] & 0x0F));       // address lower nibble
            SerXmtBuf[6] = '\r';
            SerXmtBufPtr = 7;

            // Send ASCII string to RS232 UART (Host-PC)
            UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
                                
            // Erase former CAN data bytes
            CanTxMsg.Data32[0] = 0;
            CanTxMsg.Data32[1] = 0;

            CanTxMsg.Id = 0x401;
                                
            // Set message type 29 bit
            CanTxMsg.Type = CAN_MSG_STANDARD;
	
            // set DLC
            CanTxMsg.Len = 8;
                                
            ReadMsgFromSerial();
            for (i = 0; i < 8; i++)
            {
                CanTxMsg.Data8[i] =  (ascii2hex(SerRcvBuf[i*2]) << 4);
                CanTxMsg.Data8[i] |= (ascii2hex(SerRcvBuf[i*2+1]));
            }
                                
            // send the message
            CAN_UserWrite (CAN_BUS1, &CanTxMsg);

            break;

        case 0x410: // Get reading
            // CanRxMsg.Len ignored

            SerXmtBuf[0] = '*';  // All commands begin with '*'
            SerXmtBuf[1] = '1';  // device address
            SerXmtBuf[2] = 'B';  
            SerXmtBuf[3] = '1';  // B1 = Get reading
            SerXmtBuf[4] = '\r';
            SerXmtBufPtr = 5;
                                
            // Send ASCII string to RS232 UART (Host-PC)
            UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);

            // Erase former CAN data bytes
            CanTxMsg.Data32[0] = 0;
            CanTxMsg.Data32[1] = 0;

            CanTxMsg.Id = 0x411;
                                
            // Set message type 29 bit
            CanTxMsg.Type = CAN_MSG_EXTENDED;
	
            // set DLC
            CanTxMsg.Len = 7;
                                
            ReadMsgFromSerial();
            for (i = 0; i < 7; i++)
            {
                CanTxMsg.Data8[i] =  SerRcvBuf[i];
            }
                                
            // send the message
            CAN_UserWrite (CAN_BUS1, &CanTxMsg);

            break;

        case 0x420: // Set Dout
            // CanRxMsg.Len ignored

            outbuf = CanRxMsg.Data8[0];
            HW_SetDOUT(&outbuf);
                                
            break;

        case 0x430: // Read Din
            // CanRxMsg.Len ignored

            outbuf = CanRxMsg.Data8[0];
            HW_GetDIN(&inbuf);

            // Erase former CAN data bytes
            CanTxMsg.Data32[0] = 0;
            CanTxMsg.Data32[1] = 0;

            CanTxMsg.Id = 0x431;
                                
            // Set message type 11 bit
            CanTxMsg.Type = CAN_MSG_STANDARD;
	
            // set DLC
            CanTxMsg.Len = 1;
                                
            CanTxMsg.Data8[0] = inbuf & 0xFF;
                                
            // send the message
            CAN_UserWrite (CAN_BUS1, &CanTxMsg);
                    
            break;

        case 0x440: // Set SST_LV Relay 1
            // CanRxMsg.Len ignored

            outbuf = (CanRxMsg.Data8[0] & 0x01);
            if (outbuf == 1)  // Relay on
            {
                RelayState = RelayState | 0x40;
            }
            else  // Relay off
            {
                RelayState = RelayState & ~0x40;
            }

            // Alarm config 1
            SerXmtBuf[0] = '*';  // All commands begin with '*'
            SerXmtBuf[1] = '1';  // device address
            SerXmtBuf[2] = 'Q';  // Write Upper RAM
            SerXmtBuf[3] = '1';  // Length: 1 bytes
            SerXmtBuf[4] = '0';
            SerXmtBuf[5] = 'A';  // Address 0x0A
            SerXmtBuf[6] = hex2ascii((RelayState & 0xFF) >> 4);
            SerXmtBuf[7] = '0';
            SerXmtBuf[8] = '\r';
            SerXmtBufPtr = 9;

            // Send ASCII string to RS232 UART (Host-PC)
            UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
                                
            break;

        case 0x441: // Set SST_LV Relay 2
            // CanRxMsg.Len ignored

            outbuf = (CanRxMsg.Data8[0] & 0x01);
            if (outbuf == 1)  // Relay on
            {
                RelayState = RelayState | 0x80;
            }
            else  // Relay off
            {
                RelayState = RelayState & ~0x80;
            }

            // Alarm config 1
            SerXmtBuf[0] = '*';  // All commands begin with '*'
            SerXmtBuf[1] = '1';  // device address
            SerXmtBuf[2] = 'Q';  // Write Upper RAM
            SerXmtBuf[3] = '1';  // Length: 1 bytes
            SerXmtBuf[4] = '0';
            SerXmtBuf[5] = 'A';  // Address 0x0A
            SerXmtBuf[6] = hex2ascii((RelayState & 0xFF) >> 4);
            SerXmtBuf[7] = '0';
            SerXmtBuf[8] = '\r';
            SerXmtBufPtr = 9;

            // Send ASCII string to RS232 UART (Host-PC)
            UARTWriteResult = SER_Write ( SER_PORT1, &SerXmtBuf[0], SerXmtBufPtr);
                                
            break;

        case 0x450: // Get version data
            // Erase former CAN data bytes
            CanTxMsg.Data32[0] = 0;
            CanTxMsg.Data32[1] = 0;

            CanTxMsg.Id = 0x451;
                                
            // Set message type 11 bit
            CanTxMsg.Type = CAN_MSG_STANDARD;
	
            // set DLC
            CanTxMsg.Len = 4;
                                
            CanTxMsg.Data8[0] = HwVersionHi;
            CanTxMsg.Data8[1] = HwVersionLo;
            CanTxMsg.Data8[2] = SwVersionHi;
            CanTxMsg.Data8[3] = SwVersionLo;
                                
            // send the message
            CAN_UserWrite (CAN_BUS1, &CanTxMsg);
            break;

        } // switch
        //-------------------------------------------------
    } // IF Can read
}
/**************************************************/



//*************************************************
//! @brief	Sn[CR]
//! Setup with standard CAN bitrates where n is 0-8.
//!
//! @pre This command is accepted only if the CAN channel is closed. 
//!
//! @param	bitrate selector n (where n is 0-8)
//! @li	n=0 : 10 kbit/s
//! @li	n=1 : 20 kbit/s
//! @li	n=2 : 50 kbit/s
//! @li	n=3 : 100 kbit/s
//! @li	n=4 : 125 kbit/s
//! @li	n=5 : 250 kbit/s
//! @li	n=6 : 500 kbit/s
//! @li	n=7 : 800 kbit/s
//! @li	n=8 : 1 Mbit/s
//!
//! @note example: S4[CR]
//! @note Setup CAN to 125 kbit/s.
//!
//! @return	BELL (Ascii 7) for ERROR when CAN channel is open
//! @return	CR (Ascii 13) for OK
//!
static void SetCanSpeed(void) 
//-------------------------------------------------
{
	u8_t Result;
	u32_t LpcBtr; // This is the LPC2194's "CANBTR" register

	Result = ret_OK;
	
	// if CAN channel is closed
	if (EepromData.CanChnOpen == 0){ // precondition

		switch (EepromData.CanSpeed){
		case 0: 
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_10K);
			break;
		case 1: 
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_20K);
			break;
		case 2:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_50K);
			break;
		case 3:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_100K);
			break;
		case 4:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_125K);
			break;
		case 5:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_250K);
			break;
		case 6:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_500K);
			break;
		case 7:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_800K);
			break;
		case 8:
			CAN_InitChannel (CAN_BUS1, CAN_BAUD_1M);
			break;
		case 0xFF: // not a standard speed, use BTR0BTR1 instead
			// Shuffle together LPC2194's "CANBTR" register
			LpcBtr  = ((((u32_t)EepromData.Btr[0] &0x3F) +1) *6) -1; 	// BRP, bit 0..9
			LpcBtr |=   ((u32_t)EepromData.Btr[0] &0xC0) << 8;			// SJW, bit 14..15
			LpcBtr |=   ((u32_t)EepromData.Btr[1]       << 16);			// TSEG1, TSEG2, SAM
			// Write values to CAN controller
			CAN_InitChannel (CAN_BUS1, LpcBtr);
			break;
		default: 	
			Result = ret_ERROR;	// parameter out of range, no change
			break;
		}
		EepromData.CanInitialized = 1; // Now CAN channel can be opened
	}
	else{
		Result = ret_ERROR;		// CAN channel not closed, command ignored
	}	
		
	//-------------------------------------------------

	SerXmtBufPtr = 0;

	SerXmtBuf[SerXmtBufPtr] = Result;
	SerXmtBufPtr++;
	
	// Send ASCII string to RS232 UART (Host-PC)
	SER_Write (SER_PORT1, &SerXmtBuf, SerXmtBufPtr);
}
/**************************************************/





//*************************************************
//! @brief	O[CR]
//! Open the CAN channel in normal mode (sending & receiving).
//!
//! @pre This command is accepted only if the CAN channel is closed and 
//! has already been initialized with either the S or the s command.
//!
//! @param	none
//!
//! @note example: O[CR]
//! @note Open the channel, green LED starts blinking slowly 1Hz.
//!
//! @return	BELL (Ascii 7) for ERROR when CAN channel is open or uninitialized
//! @return	CR (Ascii 13) for OK
//!
static void OpenCanChannelNormal(void) 
//-------------------------------------------------
{
	u8_t Result;
	
	Result = ret_OK;
	
	// if CAN channel is closed but initialized
	if ((EepromData.CanChnOpen == 0) && (EepromData.CanInitialized == 1)) {
		// Set BUS ON and clear queues
		EepromData.CanBusMode = BUS_ON;
		CAN_SetBusMode (CAN_BUS1, EepromData.CanBusMode);
		CAN_ReInitChannel (CAN_BUS1);
		EepromData.CanChnOpen = 1;
	}
	else {
		Result = ret_ERROR;
	}
//-------------------------------------------------

	SerXmtBufPtr = 0;

	SerXmtBuf[SerXmtBufPtr] = Result;
	SerXmtBufPtr++;
	
	// Send ASCII string to RS232 UART (Host-PC)
	SER_Write (SER_PORT1, &SerXmtBuf, SerXmtBufPtr);
}
/**************************************************/



static void WriteToEeprom(u8_t ParamSetNo)
//-------------------------------------------------
{
	u8_t Result;
	
	Result = ret_OK;

	switch (ParamSetNo){
		case 0:
			// save current parameter set
			break;
	
		case 1: // save factory defaults parameter set
			EepromData.AcceptanceCode[0]= 0x00;
			EepromData.AcceptanceCode[1]= 0x00;
			EepromData.AcceptanceCode[2]= 0x00;
			EepromData.AcceptanceCode[3]= 0x00;
			EepromData.AcceptanceMask[0]= 0xFF;			// filter fully opened
			EepromData.AcceptanceMask[1]= 0xFF;
			EepromData.AcceptanceMask[2]= 0xFF;
			EepromData.AcceptanceMask[3]= 0xFF;
			EepromData.FilterMode		= 1;			// single filter mode
			EepromData.AutostartMode    = 0; 			// off
			EepromData.AutoPollAutoSend = 1; 			// on
			EepromData.SerialSpeed      = 2; 			// value 0..8, 2: 57600 bit/sec. = shipping default
			EepromData.CanSpeed         = 4; 			// Value 0..7, 4: 125kbit/sec. = shipping default
			EepromData.CanChnOpen 		= 0;			// closed
			EepromData.CanRcvTimestampOn = 1; 			// supply timestamp to RS232
			EepromData.CanInitialized	= 1;			// 0 = notInitialized, 1 = initialized
			EepromData.CanBusMode 		= BUS_ON; 		// normal
			EepromData.LedCan1Color		= HW_LED_GREEN;	// OFF, GREEN, RED, ORANGE
			EepromData.Btr[0]			= 0x03;			// Bit Timing Register BTR0
			EepromData.Btr[1]			= 0x1C;			// Bit Timing Register BTR1 (031C = 125kbit/s)
			EepromData.DeviceSN			= *DeviceSerialNo;	// currently defined in PCAN-RS-232.h
			break;

		case 2: // clear eeprom
			EepromData.AcceptanceCode[0]= 0xFF;
			EepromData.AcceptanceCode[1]= 0xFF;
			EepromData.AcceptanceCode[2]= 0xFF;
			EepromData.AcceptanceCode[3]= 0xFF;
			EepromData.AcceptanceMask[0]= 0xFF;
			EepromData.AcceptanceMask[1]= 0xFF;
			EepromData.AcceptanceMask[2]= 0xFF;
			EepromData.AcceptanceMask[3]= 0xFF;
			EepromData.FilterMode		= 0xFF;
			EepromData.AutostartMode    = 0xFF;
			EepromData.AutoPollAutoSend = 0xFF;
			EepromData.SerialSpeed      = 0xFF;
			EepromData.CanSpeed         = 0xFF;
			EepromData.CanChnOpen 		= 0xFF;
			EepromData.CanRcvTimestampOn = 0xFF;
			EepromData.CanInitialized	= 0xFF;
			EepromData.CanBusMode 		= 0xFF;
			EepromData.LedCan1Color		= 0xFF;
			EepromData.Btr[0]			= 0xFF;
			EepromData.Btr[1]			= 0xFF;
			EepromData.DeviceSN			= 0xFFFFFFFF;
			break;
	
		default:
			Result = ret_ERROR;
			break;
	}
	if (Result == ret_OK) {
		EEPROM_Write ( 0x100, &EepromData, EepromDataLength);
		EEPROM_FlushCache();
	}

	//-------------------------------------------------
	
	SerXmtBufPtr = 0;

	SerXmtBuf[SerXmtBufPtr] = Result;
	SerXmtBufPtr++;
	
	// Send ASCII string to RS232 UART (Host-PC)
	SER_Write (SER_PORT1, &SerXmtBuf, SerXmtBufPtr);
}
/**************************************************/




//*************************************************
// CheckEepromSanity
// Are all values in a plausible range ?? 
// Called only internally while INIT
//
static void CheckEepromSanity()
//-------------------------------------------------
{
	// some important values out of range ?
	if ((EepromData.AutostartMode > 2)
	||	(EepromData.SerialSpeed > 6) 			
	||	(EepromData.CanBusMode > 2)
	|| ((EepromData.CanSpeed > 8) && (EepromData.Btr[0] == 0xFF) && (EepromData.Btr[1] == 0xFF ))) {

	WriteToEeprom(1); // write factory defaults
	}
}
/**************************************************/

//---EOF
