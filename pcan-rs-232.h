#ifndef _PCANRS232_H_
#define _PCANRS232_H_

/**
 * @file 	PCAN-RS-232.h
 * @brief 	Contains global definitions.
 * @author 	StS
 * @date 	13 Feb. 2015 / GB
 * @version	1.1.1
 */

//*********************************************************************
//  Title   : PCAN-RS-232.h
//
//	Autor   : StS
//	(c)2011 PEAK-System Technik GmbH, Darmstadt
//
//  Project : PCAN-RS-232
//
//  Function:
//
//  ----------------------------------------------------------------
//
//*********************************************************************

#define HwVersionHi '1'
#define HwVersionLo '0'

// Version 1.1 - Original
// Version 2.0 - Added CAN ID 500/501: Get version info
//             - Hardcode CAN baudrate to 125K
#define SwVersionHi '2'
#define SwVersionLo '0'

#define DeviceSerialNo "0000" // ... also on the printed label 
//uchar DeviceSerialNo[4] __at 0x3f8000; // maybe to be placed in EEPROM or boot loader ??


//--------------------------------------------------
//! Structure with main control variables
typedef struct _EepromDataStruct {
	u8_t 		AcceptanceCode[4]; 	//!< defaults 0x00000000 at startup
	u8_t 		AcceptanceMask[4]; 	//!< defaults 0xFFFFFFFF at startup
	u8_t		FilterMode;			//!< 0=dual filter, 1=single filter
	u8_t  		AutostartMode; 		//!< 0/1/2, see API
	u8_t  		AutoPollAutoSend; 	//!< on/off, on by default 
	u8_t  		SerialSpeed; 		//!< value 0..8, 57600 bit/sec. by default
	u8_t  		CanSpeed; 			//!< Value 0..7, 125k by default
	u8_t  		CanChnOpen; 		//!< 0 = closed , 1 = open
	u8_t  		CanRcvTimestampOn; 	//!< 0 = no timestamp, 1 = supply timestamp
	u8_t		CanInitialized;		//!< 0 = not initialized, 1 = initialized
	u8_t  		CanBusMode; 		//!< BUS_ON, BUS_OFF, BUS_LOM
	LEDColor_t  LedCan1Color; 		//!< OFF, GREEN, RED, ORANGE
	u8_t		Btr[2];				//!< Bit Timing Register BTR0 and BTR1
	u32_t		DeviceSN;			//!< Serial No., currently not used
} EepromDataStruct;

#define EepromDataLength (sizeof(EepromDataStruct))

extern EepromDataStruct EepromData;
extern CANMsg_t  CanRxMsg, CanTxMsg;

// Routines to be called from main()
void PCANRS232_Init(void);
void ProcessMsgFromCan(void);
void ProcessMsgFromSerial(void);

#endif 
