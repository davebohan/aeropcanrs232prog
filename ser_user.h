
#ifndef _SER_USER_H_
#define _SER_USER_H_

// size for TX Fifo
#define	SER_TX_FIFO_SIZE		240

// size for RX Fifo
#define	SER_RX_FIFO_SIZE		96


// Baudrates
#define		SER_BAUD_230K4		230400
#define		SER_BAUD_115K2		115200
#define		SER_BAUD_57K6		57600
#define		SER_BAUD_38K4		38400
#define		SER_BAUD_19K2		19200
#define		SER_BAUD_9K6		9600
#define		SER_BAUD_2K4		2400


// user proto
void  SER_UserInit (u32_t SerSpeed);

u8_t 	hex2ascii(u8_t chr);
u8_t 	ascii2hex(u8_t chr);

#endif

